﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Controllers
{
    public class TriagemsController : Controller
    {
        private readonly CCIPContext _context;

        public TriagemsController(CCIPContext context)
        {
            _context = context;
        }

        // GET: Triagems
        public async Task<IActionResult> Index()
        {
            var cCIPContext = _context.Triagem.Include(t => t.CorenNavigation).Include(t => t.CpfNavigation);
            return View(await cCIPContext.ToListAsync());
        }

        // GET: Triagems/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var triagem = await _context.Triagem
                .Include(t => t.CorenNavigation)
                .Include(t => t.CpfNavigation)
                .FirstOrDefaultAsync(m => m.CodTriagem == id);
            if (triagem == null)
            {
                return NotFound();
            }

            return View(triagem);
        }

        // GET: Triagems/Create
        public IActionResult Create()
        {
            ViewData["Coren"] = new SelectList(_context.Enfermeiros, "Coren", "Coren");
            ViewData["Cpf"] = new SelectList(_context.Pacientes, "Cpf", "Cpf");
            return View();
        }

        // POST: Triagems/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CodTriagem,Cpf,Coren,DataConsulta,DescricaoPaciente,Prioridade")] Triagem triagem)
        {
            if (ModelState.IsValid)
            {
                _context.Add(triagem);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Coren"] = new SelectList(_context.Enfermeiros, "Coren", "Coren", triagem.Coren);
            ViewData["Cpf"] = new SelectList(_context.Pacientes, "Cpf", "Cpf", triagem.Cpf);
            return View(triagem);
        }

        // GET: Triagems/Edit/5
        // POST: Triagems/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CodTriagem,Cpf,Coren,DataConsulta,DescricaoPaciente,Prioridade")] Triagem triagem)
        {
            if (id != triagem.CodTriagem)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(triagem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TriagemExists(triagem.CodTriagem))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Coren"] = new SelectList(_context.Enfermeiros, "Coren", "Coren", triagem.Coren);
            ViewData["Cpf"] = new SelectList(_context.Pacientes, "Cpf", "Cpf", triagem.Cpf);
            return View(triagem);
        }

        // GET: Triagems/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var triagem = await _context.Triagem
                .Include(t => t.CorenNavigation)
                .Include(t => t.CpfNavigation)
                .FirstOrDefaultAsync(m => m.CodTriagem == id);
            if (triagem == null)
            {
                return NotFound();
            }

            return View(triagem);
        }

        // POST: Triagems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var triagem = await _context.Triagem.FindAsync(id);
            _context.Triagem.Remove(triagem);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TriagemExists(int id)
        {
            return _context.Triagem.Any(e => e.CodTriagem == id);
        }
    }
}
