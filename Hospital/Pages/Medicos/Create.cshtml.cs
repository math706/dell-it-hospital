﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Hospital.Models;

namespace Hospital.Pages.Medicos
{
    public class CreateModel : PageModel
    {
        private readonly Hospital.Models.CCIPContext _context;

        public CreateModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["CodEspecialidade"] = new SelectList(_context.Especialidades, "CodEspecialidade", "Descricao");
            return Page();
        }

        [BindProperty]
        public Models.Medicos Medicos { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Medicos.Add(Medicos);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
