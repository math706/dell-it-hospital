﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Pages.Medicos
{
    public class EditModel : PageModel
    {
        private readonly Hospital.Models.CCIPContext _context;

        public EditModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Models.Medicos Medicos { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Medicos = await _context.Medicos
                .Include(m => m.CodEspecialidadeNavigation).FirstOrDefaultAsync(m => m.Crm == id);

            if (Medicos == null)
            {
                return NotFound();
            }
           ViewData["CodEspecialidade"] = new SelectList(_context.Especialidades, "CodEspecialidade", "Descricao");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Medicos).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicosExists(Medicos.Crm))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool MedicosExists(string id)
        {
            return _context.Medicos.Any(e => e.Crm == id);
        }
    }
}
