﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Pages.Medicos
{
    public class IndexModel : PageModel
    {

        private readonly Hospital.Models.CCIPContext _context;

        public IndexModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        public IList<Models.Medicos> Medicos { get;set; }

        public async Task OnGetAsync()
        {
            Medicos = await _context.Medicos
                .Include(m => m.CodEspecialidadeNavigation).ToListAsync();
        }
    }
}
