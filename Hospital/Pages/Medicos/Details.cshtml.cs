﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Pages.Medicos
{
    public class DetailsModel : PageModel
    {
        private readonly Hospital.Models.CCIPContext _context;

        public DetailsModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        public Models.Medicos Medicos { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Medicos = await _context.Medicos
                .Include(m => m.CodEspecialidadeNavigation).FirstOrDefaultAsync(m => m.Crm == id);

            if (Medicos == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
