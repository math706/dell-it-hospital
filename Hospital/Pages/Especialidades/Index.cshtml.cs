﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Pages.Especialidades
{
    public class IndexModel : PageModel
    {
        private readonly Hospital.Models.CCIPContext _context;

        public IndexModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        public IList<Models.Especialidades> Especialidades { get;set; }

        public async Task OnGetAsync()
        {
            Especialidades = await _context.Especialidades.ToListAsync();
        }
    }
}
