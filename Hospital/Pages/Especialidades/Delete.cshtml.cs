﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Pages.Especialidades
{
    public class DeleteModel : PageModel
    {
        private readonly Hospital.Models.CCIPContext _context;

        public DeleteModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Models.Especialidades Especialidades { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Especialidades = await _context.Especialidades.FirstOrDefaultAsync(m => m.CodEspecialidade == id);

            if (Especialidades == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Especialidades = await _context.Especialidades.FindAsync(id);

            if (Especialidades != null)
            {
                _context.Especialidades.Remove(Especialidades);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
