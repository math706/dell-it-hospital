﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Pages.Especialidades
{
    public class EditModel : PageModel
    {
        private readonly Hospital.Models.CCIPContext _context;

        public EditModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Models.Especialidades Especialidades { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Especialidades = await _context.Especialidades.FirstOrDefaultAsync(m => m.CodEspecialidade == id);

            if (Especialidades == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Especialidades).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EspecialidadesExists(Especialidades.CodEspecialidade))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool EspecialidadesExists(int id)
        {
            return _context.Especialidades.Any(e => e.CodEspecialidade == id);
        }
    }
}
