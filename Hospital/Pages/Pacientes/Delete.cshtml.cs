﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Pages.Pacientes
{
    public class DeleteModel : PageModel
    {
        private readonly Hospital.Models.CCIPContext _context;

        public DeleteModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Models.Pacientes Pacientes { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Pacientes = await _context.Pacientes.FirstOrDefaultAsync(m => m.Cpf == id);

            if (Pacientes == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Pacientes = await _context.Pacientes.FindAsync(id);

            if (Pacientes != null)
            {
                _context.Pacientes.Remove(Pacientes);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
