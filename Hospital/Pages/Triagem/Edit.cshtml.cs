﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Pages.Triagem
{
    public class EditModel : PageModel
    {
        private readonly Hospital.Models.CCIPContext _context;

        public EditModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Models.Triagem Triagem { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Triagem = await _context.Triagem
                .Include(t => t.CorenNavigation)
                .Include(t => t.CpfNavigation).FirstOrDefaultAsync(m => m.CodTriagem == id);

            if (Triagem == null)
            {
                return NotFound();
            }
           ViewData["Coren"] = new SelectList(_context.Enfermeiros, "Coren", "Coren");
           ViewData["Cpf"] = new SelectList(_context.Pacientes, "Cpf", "Cpf");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Triagem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TriagemExists(Triagem.CodTriagem))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool TriagemExists(int id)
        {
            return _context.Triagem.Any(e => e.CodTriagem == id);
        }
    }
}
