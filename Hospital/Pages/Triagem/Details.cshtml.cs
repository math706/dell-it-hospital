﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Pages.Triagem
{
    public class DetailsModel : PageModel
    {
        private readonly Hospital.Models.CCIPContext _context;

        public DetailsModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        public Models.Triagem Triagem { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Triagem = await _context.Triagem
                .Include(t => t.CorenNavigation)
                .Include(t => t.CpfNavigation).FirstOrDefaultAsync(m => m.CodTriagem == id);

            if (Triagem == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
