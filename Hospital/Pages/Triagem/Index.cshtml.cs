﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Pages.Triagem
{
    public class IndexModel : PageModel
    {
        private readonly Hospital.Models.CCIPContext _context;

        public IndexModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        public IList<Models.Triagem> Triagem { get;set; }

        public async Task OnGetAsync()
        {
            Triagem = await _context.Triagem
                .Include(t => t.CorenNavigation)
                .Include(t => t.CpfNavigation).ToListAsync();
        }
    }
}
