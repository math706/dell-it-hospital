﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Pages.Enfermeiros
{
    public class DetailsModel : PageModel
    {
        private readonly Hospital.Models.CCIPContext _context;

        public DetailsModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        public Models.Enfermeiros Enfermeiros { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Enfermeiros = await _context.Enfermeiros.FirstOrDefaultAsync(m => m.Coren == id);

            if (Enfermeiros == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
