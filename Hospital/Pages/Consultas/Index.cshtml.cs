﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Pages.Consultas
{
    public class IndexModel : PageModel
    {
        private readonly Hospital.Models.CCIPContext _context;

        public IndexModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        public IList<Models.Consultas> Consultas { get; set; }


        public async Task OnGetAsync(string form_text)
        {
            if (Hospital.Pages.IndexModel.isCpf == true)
            {
                Consultas = await _context.Consultas
                .Include(c => c.CodTriagemNavigation)
                .Include(c => c.CorenNavigation)
                .Include(c => c.CpfNavigation)
                .Include(c => c.CrmNavigation).Where(m => m.Cpf.Equals(Hospital.Pages.IndexModel.cpf_crm)).ToListAsync();

            }
            else
            {
                Consultas = await _context.Consultas
                    .Include(c => c.CodTriagemNavigation)
                    .Include(c => c.CorenNavigation)
                    .Include(c => c.CpfNavigation)
                    .Include(c => c.CrmNavigation).Where(m => m.Crm.Equals(Hospital.Pages.IndexModel.cpf_crm)).ToListAsync();
            }
        }

        //public void OnGet(string form_text)
        //{
        //    String Crm = null;// = Request.Form["CPF_CRM"];
        //    if (String.IsNullOrEmpty(Crm))
        //    {
        //        Consultas = (IList<Models.Consultas>)_context.Consultas;
        //    }
        //    else
        //    {
        //        Consultas = (IList<Models.Consultas>)_context.Consultas.Where(m => m.Crm.Equals(Crm));
        //    }
        //}

    }
}