﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Hospital.Models;

namespace Hospital.Pages.Consultas
{
    public class DetailsModel : PageModel
    {
        private readonly Hospital.Models.CCIPContext _context;

        public DetailsModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        public Models.Consultas Consultas { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Consultas = await _context.Consultas
                .Include(c => c.CodTriagemNavigation)
                .Include(c => c.CorenNavigation)
                .Include(c => c.CpfNavigation)
                .Include(c => c.CrmNavigation).FirstOrDefaultAsync(m => m.CodConsultas == id);

            if (Consultas == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
