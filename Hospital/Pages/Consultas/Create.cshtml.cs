﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Hospital.Models;

namespace Hospital.Pages.Consultas
{
    public class CreateModel : PageModel
    {
        private readonly Hospital.Models.CCIPContext _context;

        public CreateModel(Hospital.Models.CCIPContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["CodTriagem"] = new SelectList(_context.Triagem, "CodTriagem", "Coren");
        ViewData["Coren"] = new SelectList(_context.Enfermeiros, "Coren", "Coren");
        ViewData["Cpf"] = new SelectList(_context.Pacientes, "Cpf", "Cpf");
        ViewData["Crm"] = new SelectList(_context.Medicos, "Crm", "Crm");
            return Page();
        }

        [BindProperty]
        public Models.Consultas Consultas { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Consultas.Add(Consultas);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
