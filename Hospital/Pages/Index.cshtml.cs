﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Specialized;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
namespace Hospital.Pages
{
	public class IndexModel : PageModel
	{
		public static string cpf_crm = null;
		public static bool isCpf = false;
		private readonly ILogger<IndexModel> _logger;

		public IndexModel(ILogger<IndexModel> logger)
		{
			_logger = logger;
		}

		public void OnGet()
		{

		}

		public async Task<IActionResult> OnPostAsync(string form_text)
		{
			//string nome = Request.Form["Nome"];
			cpf_crm = Request.Form["CPF_CRM"];
			isCpf = false;
			if (String.IsNullOrEmpty(cpf_crm))
			{
				return RedirectToPage("./Index");
			}
			if (cpf_crm.Length == 14)
			{
				isCpf = false;
			}
			else if (cpf_crm.Length == 11)
			{
				isCpf = true;
			}

			return RedirectToPage("./Consultas/Index");


		}
	}
}