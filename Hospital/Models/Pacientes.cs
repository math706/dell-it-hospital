﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hospital.Models
{
    public partial class Pacientes
    {
        public Pacientes()
        {
            Consultas = new HashSet<Consultas>();
            Triagem = new HashSet<Triagem>();
        }

        [Key]
        [Column("CPF")]
        [StringLength(11)]
        public string Cpf { get; set; }
        [Required]
        [Column("NOME")]
        [StringLength(50)]
        public string Nome { get; set; }
        [Required]
        [Column("SEXO")]
        [StringLength(1)]
        public string Sexo { get; set; }

        [InverseProperty("CpfNavigation")]
        public virtual ICollection<Consultas> Consultas { get; set; }
        [InverseProperty("CpfNavigation")]
        public virtual ICollection<Triagem> Triagem { get; set; }
    }
}
