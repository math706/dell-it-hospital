﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hospital.Models
{
    public partial class Triagem
    {
        public Triagem()
        {
            Consultas = new HashSet<Consultas>();
        }

        [Key]
        [Column("Cod_triagem")]
        public int CodTriagem { get; set; }
        [Required]
        [Column("CPF")]
        [StringLength(11)]
        public string Cpf { get; set; }
        [Required]
        [StringLength(14)]
        public string Coren { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime DataConsulta { get; set; }
        [StringLength(50)]
        public string DescricaoPaciente { get; set; }
        [Column("prioridade", TypeName = "numeric(1, 0)")]
        public decimal? Prioridade { get; set; }

        [ForeignKey(nameof(Coren))]
        [InverseProperty(nameof(Enfermeiros.Triagem))]
        public virtual Enfermeiros CorenNavigation { get; set; }
        [ForeignKey(nameof(Cpf))]
        [InverseProperty(nameof(Pacientes.Triagem))]
        public virtual Pacientes CpfNavigation { get; set; }
        [InverseProperty("CodTriagemNavigation")]
        public virtual ICollection<Consultas> Consultas { get; set; }
    }
}
