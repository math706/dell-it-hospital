﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hospital.Models
{
    public partial class Medicos
    {
        public Medicos()
        {
            Consultas = new HashSet<Consultas>();
        }

        [Key]
        [Column("CRM")]
        [StringLength(14)]
        public string Crm { get; set; }
        [Required]
        [Column("NOME")]
        [StringLength(50)]
        public string Nome { get; set; }
        [Column("Cod_especialidade")]
        public int CodEspecialidade { get; set; }

        [ForeignKey(nameof(CodEspecialidade))]
        [InverseProperty(nameof(Especialidades.Medicos))]
        public virtual Especialidades CodEspecialidadeNavigation { get; set; }
        [InverseProperty("CrmNavigation")]
        public virtual ICollection<Consultas> Consultas { get; set; }
    }
}
