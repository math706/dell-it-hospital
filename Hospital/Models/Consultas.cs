﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hospital.Models
{
    public partial class Consultas
    {
        [Required]
        [Column("CPF")]
        [StringLength(11)]
        public string Cpf { get; set; }
        [Required]
        [Column("CRM")]
        [StringLength(14)]
        public string Crm { get; set; }
        [StringLength(14)]
        public string Coren { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime DataConsulta { get; set; }
        [Column("Cod_Triagem")]
        public int? CodTriagem { get; set; }
        [Key]
        [Column("Cod_Consultas")]
        public int CodConsultas { get; set; }

        [ForeignKey(nameof(CodTriagem))]
        [InverseProperty(nameof(Triagem.Consultas))]
        public virtual Triagem CodTriagemNavigation { get; set; }
        [ForeignKey(nameof(Coren))]
        [InverseProperty(nameof(Enfermeiros.Consultas))]
        public virtual Enfermeiros CorenNavigation { get; set; }
        [ForeignKey(nameof(Cpf))]
        [InverseProperty(nameof(Pacientes.Consultas))]
        public virtual Pacientes CpfNavigation { get; set; }
        [ForeignKey(nameof(Crm))]
        [InverseProperty(nameof(Medicos.Consultas))]
        public virtual Medicos CrmNavigation { get; set; }
    }
}
