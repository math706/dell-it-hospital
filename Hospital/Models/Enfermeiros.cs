﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hospital.Models
{
    public partial class Enfermeiros
    {
        public Enfermeiros()
        {
            Consultas = new HashSet<Consultas>();
            Triagem = new HashSet<Triagem>();
        }

        [Key]
        [Column("COREN")]
        [StringLength(14)]
        public string Coren { get; set; }
        [Required]
        [Column("NOME")]
        [StringLength(50)]
        public string Nome { get; set; }

        [InverseProperty("CorenNavigation")]
        public virtual ICollection<Consultas> Consultas { get; set; }
        [InverseProperty("CorenNavigation")]
        public virtual ICollection<Triagem> Triagem { get; set; }
    }
}
