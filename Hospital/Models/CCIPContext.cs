﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Hospital.Models
{
    public partial class CCIPContext : DbContext
    {
        public CCIPContext()
        {
        }

        public CCIPContext(DbContextOptions<CCIPContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Consultas> Consultas { get; set; }
        public virtual DbSet<Enfermeiros> Enfermeiros { get; set; }
        public virtual DbSet<Especialidades> Especialidades { get; set; }
        public virtual DbSet<Medicos> Medicos { get; set; }
        public virtual DbSet<Pacientes> Pacientes { get; set; }
        public virtual DbSet<Triagem> Triagem { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-RAPP2K0\\MYSQLSERV;initial catalog=ultimo hospital;trusted_connection=true;initial catalog=DESKTOP-RAPP2K0\\MYSQLSERV;initial catalog=ultimo hospital;trusted_connection=true;Integrated Security=True;ConnectRetryCount=0;MultipleActiveResultSets=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Consultas>(entity =>
            {
                entity.HasKey(e => e.CodConsultas)
                    .HasName("PK__Consulta__AB89BEABEE682414");

                entity.HasIndex(e => new { e.Cpf, e.DataConsulta, e.Crm, e.Coren, e.CodTriagem })
                    .HasName("ck_marcado")
                    .IsUnique();

                entity.Property(e => e.Coren).IsUnicode(false);

                entity.Property(e => e.Cpf).IsUnicode(false);

                entity.Property(e => e.Crm).IsUnicode(false);

                entity.HasOne(d => d.CodTriagemNavigation)
                    .WithMany(p => p.Consultas)
                    .HasForeignKey(d => d.CodTriagem)
                    .HasConstraintName("FK_TRIAGEM");

                entity.HasOne(d => d.CorenNavigation)
                    .WithMany(p => p.Consultas)
                    .HasForeignKey(d => d.Coren)
                    .HasConstraintName("FK_ENFERMEIROS");

                entity.HasOne(d => d.CpfNavigation)
                    .WithMany(p => p.Consultas)
                    .HasForeignKey(d => d.Cpf)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PACIENTES");

                entity.HasOne(d => d.CrmNavigation)
                    .WithMany(p => p.Consultas)
                    .HasForeignKey(d => d.Crm)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MEDICOS");
            });

            modelBuilder.Entity<Enfermeiros>(entity =>
            {
                entity.HasKey(e => e.Coren)
                    .HasName("PK__Enfermei__F7D2C3E9F0B457BB");

                entity.Property(e => e.Coren).IsUnicode(false);

                entity.Property(e => e.Nome).IsUnicode(false);
            });

            modelBuilder.Entity<Especialidades>(entity =>
            {
                entity.HasKey(e => e.CodEspecialidade)
                    .HasName("PK__Especial__0EB6E85505934817");

                entity.HasIndex(e => e.Nome)
                    .HasName("UQ__Especial__7D8FE3B28F12DD7E")
                    .IsUnique();

                entity.Property(e => e.Descricao).IsUnicode(false);

                entity.Property(e => e.Nome).IsUnicode(false);
            });

            modelBuilder.Entity<Medicos>(entity =>
            {
                entity.HasKey(e => e.Crm)
                    .HasName("PK__Medicos__C1F887FE8775EB6F");

                entity.Property(e => e.Crm).IsUnicode(false);

                entity.Property(e => e.Nome).IsUnicode(false);

                entity.HasOne(d => d.CodEspecialidadeNavigation)
                    .WithMany(p => p.Medicos)
                    .HasForeignKey(d => d.CodEspecialidade)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ESPECIALIDADES");
            });

            modelBuilder.Entity<Pacientes>(entity =>
            {
                entity.HasKey(e => e.Cpf)
                    .HasName("PK__Paciente__C1F8973049E13ADE");

                entity.Property(e => e.Cpf).IsUnicode(false);

                entity.Property(e => e.Nome).IsUnicode(false);

                entity.Property(e => e.Sexo)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<Triagem>(entity =>
            {
                entity.HasKey(e => e.CodTriagem)
                    .HasName("PK__Triagem__49128E5680D87098");

                entity.Property(e => e.Coren).IsUnicode(false);

                entity.Property(e => e.Cpf).IsUnicode(false);

                entity.Property(e => e.DescricaoPaciente).IsUnicode(false);

                entity.HasOne(d => d.CorenNavigation)
                    .WithMany(p => p.Triagem)
                    .HasForeignKey(d => d.Coren)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_COREN");

                entity.HasOne(d => d.CpfNavigation)
                    .WithMany(p => p.Triagem)
                    .HasForeignKey(d => d.Cpf)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CPF");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
