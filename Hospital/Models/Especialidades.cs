﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hospital.Models
{
    public partial class Especialidades
    {
        public Especialidades()
        {
            Medicos = new HashSet<Medicos>();
        }

        [Required]
        [StringLength(20)]
        public string Nome { get; set; }
        [Key]
        [Column("Cod_especialidade")]
        public int CodEspecialidade { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public decimal ValorConsulta { get; set; }
        [Required]
        [StringLength(100)]
        public string Descricao { get; set; }

        [InverseProperty("CodEspecialidadeNavigation")]
        public virtual ICollection<Medicos> Medicos { get; set; }
    }
}
