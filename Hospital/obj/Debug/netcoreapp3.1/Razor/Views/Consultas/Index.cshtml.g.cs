#pragma checksum "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "037ecbc7f644269ba0317d36ed0a9ec7bf9f1ee6"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Consultas_Index), @"mvc.1.0.view", @"/Views/Consultas/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"037ecbc7f644269ba0317d36ed0a9ec7bf9f1ee6", @"/Views/Consultas/Index.cshtml")]
    public class Views_Consultas_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Hospital.Models.Consultas>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
  
    ViewData["Title"] = "Index";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>Index</h1>\r\n\r\n<p>\r\n    <a asp-action=\"Create\">Create New</a>\r\n</p>\r\n<table class=\"table\">\r\n    <thead>\r\n        <tr>\r\n            <th>\r\n                ");
#nullable restore
#line 16 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.DataConsulta));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 19 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.CodTriagemNavigation));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 22 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.CorenNavigation));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 25 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.CpfNavigation));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 28 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.CrmNavigation));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th></th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n");
#nullable restore
#line 34 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
 foreach (var item in Model) {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <tr>\r\n            <td>\r\n                ");
#nullable restore
#line 37 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.DataConsulta));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 40 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.CodTriagemNavigation.Coren));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 43 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.CorenNavigation.Coren));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 46 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.CpfNavigation.Cpf));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 49 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.CrmNavigation.Crm));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                <a asp-action=\"Edit\"");
            BeginWriteAttribute("asp-route-id", " asp-route-id=\"", 1460, "\"", 1493, 1);
#nullable restore
#line 52 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
WriteAttributeValue("", 1475, item.CodConsultas, 1475, 18, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Edit</a> |\r\n                <a asp-action=\"Details\"");
            BeginWriteAttribute("asp-route-id", " asp-route-id=\"", 1546, "\"", 1579, 1);
#nullable restore
#line 53 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
WriteAttributeValue("", 1561, item.CodConsultas, 1561, 18, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Details</a> |\r\n                <a asp-action=\"Delete\"");
            BeginWriteAttribute("asp-route-id", " asp-route-id=\"", 1634, "\"", 1667, 1);
#nullable restore
#line 54 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
WriteAttributeValue("", 1649, item.CodConsultas, 1649, 18, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Delete</a>\r\n            </td>\r\n        </tr>\r\n");
#nullable restore
#line 57 "C:\Users\mathi\Desktop\dell_Hospital\Hospital\Views\Consultas\Index.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("    </tbody>\r\n</table>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Hospital.Models.Consultas>> Html { get; private set; }
    }
}
#pragma warning restore 1591
