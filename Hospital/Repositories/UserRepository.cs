﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hospital.Models;

namespace Hospital.Repositories
{
    public static class UserRepository
    {
        public static Medicos Get(string umCrm, string umNome) 
        {
            Medicos umMedico = new Medicos();

            using CCIPContext context = new CCIPContext();
            if (umCrm.Length == 7)
            {
                var medico = context.Medicos
                .Where(m => m.Crm.Equals(umCrm))
                .FirstOrDefault<Medicos>();

                

                return medico;
            }
            else return null;
        }  
    }
}
