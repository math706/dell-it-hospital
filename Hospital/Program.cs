using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Hospital.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Hospital
{
	public class Program
	{
		public static void Main(string[] args)
		{

			Console.WriteLine("Pesquisar por interface grafica(I) ou por terminal(T)?");
			String begin_text = Console.ReadLine();
			if (begin_text.Equals("I"))
			{
				CreateHostBuilder(args).Build().Run();
			}
			else
			{
				while (true)
				{
					Console.WriteLine("informe um cpf ou crm: (Digite SAIR para encerrar)");
					String thisText = Console.ReadLine();
					if (thisText.Equals("SAIR")) break;
					using CCIPContext context = new CCIPContext();
					if (thisText.Length == 11)
					{
						var this_consulta_info = context.Consultas
							.Where(m => m.Cpf.Equals(thisText));

						var this_paciente_nome = context.Pacientes
								.Where(m => m.Cpf.Equals(thisText));
						foreach (Pacientes one_paciente in this_paciente_nome) { Console.WriteLine($"Paciente CPF: {thisText} Nome {one_paciente.Nome}"); }


						foreach (Consultas one_consulta in this_consulta_info)
						{
							Console.WriteLine($"-------------------------");
							var this_medico_nome = context.Medicos
									.Where(m => m.Crm.Equals(one_consulta.Crm));
							var this_enfermeiro_nome = context.Enfermeiros
								.Where(m => m.Coren.Equals(one_consulta.Coren));
							foreach (Medicos one_medico_nome in this_medico_nome) { Console.WriteLine($"Medico CRM: {one_consulta.Crm} - Nome {one_medico_nome.Nome}"); }
							foreach (Enfermeiros one_enfermeiro_nome in this_enfermeiro_nome) { Console.WriteLine($"Enfermeiro Coren: {one_consulta.Coren} Nome {one_enfermeiro_nome.Nome}"); }

							Console.WriteLine($"Data: {one_consulta.DataConsulta}");
							Console.WriteLine($"Codigo: {one_consulta.CodTriagem}");
							Console.WriteLine($"\n");
						}
					}
					else
					{

						var this_consulta_info = context.Consultas
							.Where(m => m.Crm.Equals(thisText));

						var this_medico_nome = context.Medicos
								.Where(m => m.Crm.Equals(thisText));
						foreach (Medicos one_medico in this_medico_nome) { Console.WriteLine($"Medico CRM: {thisText} Nome {one_medico.Nome}"); }


						foreach (Consultas one_consulta in this_consulta_info)
						{
							Console.WriteLine($"-------------------------");
							var this_paciente_nome = context.Pacientes
									.Where(m => m.Cpf.Equals(one_consulta.Cpf));
							foreach (Pacientes one_paciente_nome in this_paciente_nome)
							{
								if (one_consulta.Equals(null)){ }
								Console.WriteLine($"Paciente CPF: {one_consulta.Cpf} - Nome {one_paciente_nome.Nome}");
							}
							var this_enfermeiro_nome = context.Enfermeiros
								.Where(m => m.Coren.Equals(one_consulta.Coren));
							
							foreach (Enfermeiros one_enfermeiro_nome in this_enfermeiro_nome) {
								if (one_consulta.Equals(null)){ }
								Console.WriteLine($"Enfermeiro Coren: {one_consulta.Coren} Nome {one_enfermeiro_nome.Nome}"); 
							}

							Console.WriteLine($"Data: {one_consulta.DataConsulta}");
							Console.WriteLine($"Codigo: {one_consulta.CodTriagem}");
							Console.WriteLine($"\n");
						}


					}
				}
			}

		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseStartup<Startup>();
				});


	}
}